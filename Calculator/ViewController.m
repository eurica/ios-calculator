//
//  ViewController.m
//  Calculator
//
//  Created by Christian Larazo on 2/18/15.
//  Copyright (c) 2015 Larazo Techworks. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstNumber;
@property (weak, nonatomic) IBOutlet UITextField *operation;
@property (weak, nonatomic) IBOutlet UITextField *secondNumber;
@property (weak, nonatomic) IBOutlet UITextField *output;

@end

@implementation ViewController

// First number flag
bool isFirst = true;

- (void)viewDidLoad {
    [super viewDidLoad];

    // Dismiss key board
    UIGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Dismiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender {
    [self.view endEditing:YES];
}

// Set number to view
- (void) setNumber: (NSString*)num {
    if (isFirst) {
        _firstNumber.text = [_firstNumber.text stringByAppendingString:num];
    } else {
        _secondNumber.text = [_secondNumber.text stringByAppendingString:num];
    }
}

// Set operation
- (void) setOperations: (NSString*)op {
    _operation.text = op;
    isFirst = false;
}


#pragma mark - Number key events
- (IBAction)zeroClick:(id)sender {
    [self setNumber:@"0"];
}
- (IBAction)oneClick:(id)sender {
    [self setNumber:@"1"];
}
- (IBAction)twoClick:(id)sender {
    [self setNumber:@"2"];
}
- (IBAction)threeClick:(id)sender {
    [self setNumber:@"3"];
}
- (IBAction)fourClick:(id)sender {
    [self setNumber:@"4"];
}
- (IBAction)fiveClick:(id)sender {
    [self setNumber:@"5"];
}
- (IBAction)sixClick:(id)sender {
    [self setNumber:@"6"];
}
- (IBAction)sevenClick:(id)sender {
    [self setNumber:@"7"];
}
- (IBAction)eightClick:(id)sender {
    [self setNumber:@"8"];
}
- (IBAction)nineClick:(id)sender {
    [self setNumber:@"9"];
}


#pragma mark - Operation events
- (IBAction)plus:(id)sender {
    [self setOperations:@"+"];
}
- (IBAction)minus:(id)sender {
    [self setOperations:@"-"];
}
- (IBAction)multiply:(id)sender {
    [self setOperations:@"x"];
}
- (IBAction)divide:(id)sender {
    [self setOperations:@"/"];
}


#pragma mark - Other events
- (IBAction)decimal:(id)sender {
    if (isFirst) {
        // Search from input if there is decimal point
        if ([_firstNumber.text rangeOfString:@"."].location == NSNotFound) {
            [self setNumber:@"."];
        }
    } else {
        if ([_secondNumber.text rangeOfString:@"."].location == NSNotFound) {
            [self setNumber:@"."];
        }
    }
}

- (IBAction)equal:(id)sender {
    // Validate inputs
    if (![_firstNumber.text isEqualToString:@""] && ![_secondNumber.text isEqualToString:@""] && ![_operation.text isEqualToString:@""]) {
        double firstNum = [_firstNumber.text floatValue];
        double secondNum = [_secondNumber.text floatValue];
        NSString *oper = _operation.text;
        
        double total = 0;
        
        if ([oper isEqualToString:@"+"]) {
            total = firstNum + secondNum;
        } else if ([oper isEqualToString:@"-"]) {
            total = firstNum - secondNum;
        } else if ([oper isEqualToString:@"x"]) {
            total = firstNum * secondNum;
        } else if ([oper isEqualToString:@"/"]) {
            total = firstNum / secondNum;
        }
        
        _output.text = [NSString stringWithFormat:@"%f", total];
    }
}

- (IBAction)clear:(id)sender {
    _firstNumber.text = @"";
    _secondNumber.text = @"";
    _operation.text = @"";
    _output.text = @"";
    
    isFirst = TRUE;
}

- (IBAction)delete:(id)sender {
    if (isFirst) {
        // Delete last character from string
        _firstNumber.text = [_firstNumber.text substringToIndex:[_firstNumber.text length] - 1];
    } else {
        _secondNumber.text = [_secondNumber.text substringToIndex:[_secondNumber.text length] - 1];
    }
}

- (IBAction)setPositiveNegative:(id)sender {
    if (isFirst) {
        // Check if negative sign is in string
        if ([_firstNumber.text rangeOfString:@"-"].location == NSNotFound) {
            // Delete negative sign if existing
            _firstNumber.text = [NSString stringWithFormat:@"-%@", _firstNumber.text];
        } else {
            _firstNumber.text = [_firstNumber.text substringFromIndex:1];
        }
    } else {
        if ([_secondNumber.text rangeOfString:@"-"].location == NSNotFound) {
            _secondNumber.text = [NSString stringWithFormat:@"-%@", _secondNumber.text];
        } else {
            _secondNumber.text = [_secondNumber.text substringFromIndex:1];
        }
    }
}

@end
